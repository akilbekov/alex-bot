require('dotenv').config();
const PouchDB = require('pouchdb');
PouchDB.plugin(require('pouchdb-upsert'));

const Telegraf = require('telegraf');
const axios = require('axios');

const bot = new Telegraf(process.env.BOT_TOKEN);
const db = new PouchDB('chats');

db.putIfNotExists({ _id: '-280826659', match_id: 0 });

const getLastGameResult = () =>
  axios
    .get('https://api.opendota.com/api/players/237337465/wl?limit=1')
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      console.log(error);
    });

bot.start(ctx => {
  db.putIfNotExists(ctx.chat.id.toString(), { match_id: 0 });
  return ctx.reply('Накладываю проклятие на Лешу...');
});

bot.command('remove', ctx => {
  db.get(ctx.chat.id.toString())
    .then(res => db.remove(res))
    .catch(function(err) {
      console.log('ERROR');
    });
  return ctx.reply('Ну и ладно.');
});

const lastMatchId = () =>
  axios.get('https://api.opendota.com/api/players/237337465/matches?limit=1').then(response => {
    match_id = response.data[0].match_id;
    getLastGameResult().then(data => {
      db.allDocs({ include_docs: true }).then(result => {
        if (data.win === 0) {
          result.rows.forEach(row => {
            console.log(result.rows[0]);
            console.log(row.doc.match_id, 'Match ID row');
            console.log(match_id, 'Match ID');
            if (match_id !== row.doc.match_id) {
              bot.telegram.sendMessage(
                row.id,
                `Проклятие сработало, Леша проиграл, -25 \n https://www.dotabuff.com/matches/${match_id}`
              );
              db.upsert(row.id, function(doc) {
                doc.match_id = match_id;
                return doc;
              })
                .then(function(res) {
                  // success, res is {rev: '1-xxx', updated: true, id: 'myDocId'}
                })
                .catch(function(err) {
                  console.log('error');
                });
            }
          });
        } else {
          result.rows.forEach(row => {
            if (match_id !== row.doc.match_id) {
              bot.telegram.sendMessage(
                row.id,
                `Леша выиграл \n https://www.dotabuff.com/matches/${match_id}`
              );
            }
            db.upsert(row.id, function(doc) {
              doc.match_id = match_id;
              return doc;
            })
              .then(function(res) {
                // success, res is {rev: '1-xxx', updated: true, id: 'myDocId'}
              })
              .catch(function(err) {
                console.log('error');
              });
          });
        }
      });
    });
  });

lastMatchId();

function callEvery30Minutes() {
  setInterval(lastMatchId, 1000 * 30 * 60);
}

callEvery30Minutes();

bot.startPolling();
